/* globals define, app, ajaxify, bootbox, socket, templates, utils */

$(document).ready(function () {
    'use strict';

    require([
        'composer/formatting', 'composer/controls'
    ], function (formatting, controls) {

        var tag1    = '<spoiler>',
			tag2	= '</spoiler>',
            nl     = '\n',
            prompt = 'spoiler text';

        formatting.addButtonDispatch('ns-spoiler', composerControlDidClick);

        function composerControlDidClick(textArea, selectionStart, selectionEnd) {
            if (selectionStart === selectionEnd) {
                var hlContentStart = selectionStart + tag1.length + nl.length,
                    hlContentEnd   = hlContentStart + prompt.length;
                controls.insertIntoTextarea(textArea, getNewSpoiler());
                controls.updateTextareaSelection(textArea, hlContentStart, hlContentEnd);
            } else {
                controls.wrapSelectionInTextareaWith(textArea, tag1, tag2);
            }
        }

        function getNewSpoiler() {
            return tag1 + nl + prompt + nl + tag2;
        }
    });
});
