(function (Controller) {
    'use strict';

    var spoiler          = /(^\(spoiler\)$)\s*(?:<br\s*\/?>\s*)*([\s\S]+?)(^\(/spoiler\)$)/g,
        sanitizeWrap     = /<(\w+)[^<]*>(^\(spoiler\)$)<\/\1>/g,
        safeCloseForList = /(<(ul|ol)>[\s\S]+?)(^\(spoiler\)$)([\s\S]+?<\/\2>)/g,
        safeShiftStart   = /^(<p>)(^\(spoiler\)$)$/gm,
        safeShiftEnd     = /^(^\(/spoiler\)$)(<\/p>)$/gm,
        template         = '<div class="ns-spoiler" data-open="false"><div class="ns-spoiler-control"><a class="btn btn-default" href="#"><i class="fa fa-eye"></i> spoiler</a></div><div class="ns-spoiler-content">$1</div></div>';

    /**
     * Performs replacements on content field.
     *
     * @param payload {object} - includes full post entity Payload.postData.content
     * @param callback returns updated content
     */
    Controller.parsePost = function (payload, callback) {
        var content = payload.postData.content;

        // 1. Sanitize: remove wrapping tags, like <p>
        // 2. Fix not properly closed <ul> and <ol> lists
        // 3. Fix text lines at Start, that concatenates with spoiler via paragraph
        if (content) {
            content = content
                .replace(sanitizeWrap, '$2')
                .replace(safeCloseForList, '$1$4\n$3')
                .replace(safeShiftStart, '$2$1')
                .replace(safeShiftEnd, '$2$1');
            content = content.replace(spoiler, template);
            payload.postData.content = content;
        }

        callback(null, payload);
    };

})(module.exports);